
// = = = = S C R E E N = = = = \\ 
var screenWidth = window.innerWidth
var screenHeight = window.innerHeight
document.querySelector('.playground').style.width = screenWidth+'px'
document.querySelector('.playground').style.height = screenHeight+'px'

// = = = =  PLATFORMS - SIZE  = = = = \\ 
var platform_width = screenWidth/90
var platform_height = screenHeight/6

document.querySelector('.p1').style.width = platform_width+'px'
document.querySelector('.p1').style.height = platform_height+'px'
document.querySelector('.p2').style.width = platform_width+'px'
document.querySelector('.p2').style.height = platform_height+'px'

// = = = = PLATFORMS - ALIGN = = = = \\ 
var x = screenWidth/2-platform_width/2
var y = screenHeight/2-platform_height/2
document.querySelector('.p2').style.left = screenWidth-platform_width+'px'
var y2 = screenHeight/2-platform_height/2

// = = = = BALL - SIZE = = = = \\ 
var ball_size = screenHeight/25
document.querySelector('.ball').style.width = ball_size+'px'
document.querySelector('.ball').style.height = ball_size+'px'

// = = = = BALL - ALIGN = = = = \\ 
var ball_x = screenWidth/2-ball_size/2
var ball_y = screenHeight/2-ball_size/2

// = = = = BALL - SPEED = = = = \\ 
var ball_speed = screenWidth/200

// = = = = SCORE = = = = \\ 
var p1_points = 0
var p2_points = 0

// = = = = Overtime speed increase = = = = \\ 
var time = 0
var speed_increase = 0

// = = = = Split screen LINE = = = = \\ 
var line_width = screenWidth/180
document.querySelector('.line').style.width = line_width+'px'
document.querySelector('.line').style.height = screenHeight+'px'
document.querySelector('.line').style.left = screenWidth/2-line_width/2+'px'

// = = = = Score = = = = \\ 
var score_size = screenWidth/150

// = = = = Scores = = = = \\ 
var score_box_size = screenWidth/15

document.querySelector('.sp1').style.width = score_box_size+'px'
document.querySelector('.sp1').style.height = score_box_size+'px'
document.querySelector('.sp2').style.width = score_box_size+'px'
document.querySelector('.sp2').style.height = score_box_size+'px'

document.querySelector('.sp1').style.fontSize = screenWidth/20+'px'
document.querySelector('.sp2').style.fontSize = screenWidth/20+'px'

document.querySelector('.sp1').style.left = screenWidth/2-score_box_size-score_box_size/2+'px'
document.querySelector('.sp2').style.left = screenWidth/2+score_box_size/2+'px'

document.querySelector('.sp1').style.top = screenHeight/20+'px'
document.querySelector('.sp2').style.top = screenHeight/20+'px'







